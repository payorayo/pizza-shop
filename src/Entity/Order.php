<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=OrderPizza::class, mappedBy="ordercurrent")
     */
    private $orderPizzas;

    public function __construct()
    {
        $this->orderPizzas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|OrderPizza[]
     */
    public function getOrderPizzas(): Collection
    {
        return $this->orderPizzas;
    }

    public function addOrderPizza(OrderPizza $orderPizza): self
    {
        if (!$this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas[] = $orderPizza;
            $orderPizza->setOrdercurrent($this);
        }

        return $this;
    }

    public function removeOrderPizza(OrderPizza $orderPizza): self
    {
        if ($this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas->removeElement($orderPizza);
            // set the owning side to null (unless already changed)
            if ($orderPizza->getOrdercurrent() === $this) {
                $orderPizza->setOrdercurrent(null);
            }
        }

        return $this;
    }
}
