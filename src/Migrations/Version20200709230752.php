<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200709230752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, status_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F52993986BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_pizza (id INT AUTO_INCREMENT NOT NULL, pizza_id INT NOT NULL, ordercurrent_id INT NOT NULL, size_id INT NOT NULL, amount INT NOT NULL, INDEX IDX_4C43F692D41D1D42 (pizza_id), INDEX IDX_4C43F692491A0B9 (ordercurrent_id), INDEX IDX_4C43F692498DA827 (size_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizza (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pizza_topping (pizza_id INT NOT NULL, topping_id INT NOT NULL, INDEX IDX_26454CADD41D1D42 (pizza_id), INDEX IDX_26454CADE9C2067C (topping_id), PRIMARY KEY(pizza_id, topping_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE size (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topping (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993986BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE order_pizza ADD CONSTRAINT FK_4C43F692D41D1D42 FOREIGN KEY (pizza_id) REFERENCES pizza (id)');
        $this->addSql('ALTER TABLE order_pizza ADD CONSTRAINT FK_4C43F692491A0B9 FOREIGN KEY (ordercurrent_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_pizza ADD CONSTRAINT FK_4C43F692498DA827 FOREIGN KEY (size_id) REFERENCES size (id)');
        $this->addSql('ALTER TABLE pizza_topping ADD CONSTRAINT FK_26454CADD41D1D42 FOREIGN KEY (pizza_id) REFERENCES pizza (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pizza_topping ADD CONSTRAINT FK_26454CADE9C2067C FOREIGN KEY (topping_id) REFERENCES topping (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_pizza DROP FOREIGN KEY FK_4C43F692491A0B9');
        $this->addSql('ALTER TABLE order_pizza DROP FOREIGN KEY FK_4C43F692D41D1D42');
        $this->addSql('ALTER TABLE pizza_topping DROP FOREIGN KEY FK_26454CADD41D1D42');
        $this->addSql('ALTER TABLE order_pizza DROP FOREIGN KEY FK_4C43F692498DA827');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993986BF700BD');
        $this->addSql('ALTER TABLE pizza_topping DROP FOREIGN KEY FK_26454CADE9C2067C');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_pizza');
        $this->addSql('DROP TABLE pizza');
        $this->addSql('DROP TABLE pizza_topping');
        $this->addSql('DROP TABLE size');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE topping');
    }
}
