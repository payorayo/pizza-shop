<?php

namespace App\DataFixtures;

use App\Entity\Size;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SizeFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $sizes = [
            'small',
            'medium',
            'large'
        ];
        for ($i = 0; $i < count($sizes); $i++) {
            $size = new Size();
            $size->setName($sizes[$i]);
            $manager->persist($size);
        }
        $manager->flush();
    }
}
