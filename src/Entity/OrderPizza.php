<?php

namespace App\Entity;

use App\Repository\OrderPizzaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderPizzaRepository::class)
 */
class OrderPizza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity=Pizza::class, inversedBy="orderPizzas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizza;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderPizzas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ordercurrent;

    /**
     * @ORM\ManyToOne(targetEntity=Size::class, inversedBy="orderPizzas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $size;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPizza(): ?Pizza
    {
        return $this->pizza;
    }

    public function setPizza(?Pizza $pizza): self
    {
        $this->pizza = $pizza;

        return $this;
    }

    public function getOrdercurrent(): ?Order
    {
        return $this->ordercurrent;
    }

    public function setOrdercurrent(?Order $ordercurrent): self
    {
        $this->ordercurrent = $ordercurrent;

        return $this;
    }

    public function getSize(): ?Size
    {
        return $this->size;
    }

    public function setSize(?Size $size): self
    {
        $this->size = $size;

        return $this;
    }
}
