<?php

namespace App\Entity;

use App\Repository\SizeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SizeRepository::class)
 */
class Size
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=OrderPizza::class, mappedBy="size")
     */
    private $orderPizzas;

    public function __construct()
    {
        $this->orderPizzas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|OrderPizza[]
     */
    public function getOrderPizzas(): Collection
    {
        return $this->orderPizzas;
    }

    public function addOrderPizza(OrderPizza $orderPizza): self
    {
        if (!$this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas[] = $orderPizza;
            $orderPizza->setSize($this);
        }

        return $this;
    }

    public function removeOrderPizza(OrderPizza $orderPizza): self
    {
        if ($this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas->removeElement($orderPizza);
            // set the owning side to null (unless already changed)
            if ($orderPizza->getSize() === $this) {
                $orderPizza->setSize(null);
            }
        }

        return $this;
    }

}
