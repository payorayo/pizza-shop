<?php

namespace App\Entity;

use App\Repository\PizzaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PizzaRepository::class)
 */
class Pizza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Topping::class, inversedBy="pizzas")
     */
    private $toppings;

    /**
     * @ORM\OneToMany(targetEntity=OrderPizza::class, mappedBy="pizza")
     */
    private $orderPizzas;

    public function __construct()
    {
        $this->toppings = new ArrayCollection();
        $this->orderPizzas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Topping[]
     */
    public function getToppings(): Collection
    {
        return $this->toppings;
    }

    public function addTopping(Topping $topping): self
    {
        if (!$this->toppings->contains($topping)) {
            $this->toppings[] = $topping;
        }

        return $this;
    }

    public function removeTopping(Topping $topping): self
    {
        if ($this->toppings->contains($topping)) {
            $this->toppings->removeElement($topping);
        }

        return $this;
    }

    /**
     * @return Collection|OrderPizza[]
     */
    public function getOrderPizzas(): Collection
    {
        return $this->orderPizzas;
    }

    public function addOrderPizza(OrderPizza $orderPizza): self
    {
        if (!$this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas[] = $orderPizza;
            $orderPizza->setPizza($this);
        }

        return $this;
    }

    public function removeOrderPizza(OrderPizza $orderPizza): self
    {
        if ($this->orderPizzas->contains($orderPizza)) {
            $this->orderPizzas->removeElement($orderPizza);
            // set the owning side to null (unless already changed)
            if ($orderPizza->getPizza() === $this) {
                $orderPizza->setPizza(null);
            }
        }

        return $this;
    }
}
