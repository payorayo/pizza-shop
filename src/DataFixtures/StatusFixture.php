<?php

namespace App\DataFixtures;

use App\Entity\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StatusFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $statusPizza = [
            'waiting to cook',
            'cooking',
            'finished'
        ];

        for ($i = 0; $i < count($statusPizza); $i++) {
            $status = new Status();
            $status->setName($statusPizza[$i]);
            $manager->persist($status);
        }

        $manager->flush();
    }
}
