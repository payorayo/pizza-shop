<?php


namespace App\Service;


class Constants
{
    const STATUS_DEFAULT = 1;

    CONST ID_ERROR = "The id does not exist";

    CONST PIZZAS_ERROR = "There are some errors in the pizzas node";

    CONST STATUS_ERROR = "The status_id does not exist";
}