<?php

namespace App\Controller;

use App\Service\OrderService;
use App\Entity\Order;
use App\Entity\OrderPizza;
use App\Repository\OrderPizzaRepository;
use App\Repository\OrderRepository;
use App\Repository\PizzaRepository;
use App\Repository\SizeRepository;
use App\Repository\StatusRepository;
use Carbon\Carbon;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends FOSRestController
{
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @Rest\Get("/v1/order/{id}.{_format}", name="board_list", defaults={"_format":"json"})
     * @param int $id
     * @return Response
     */
    public function getOrder($id)
    {
        $serializer = $this->get('jms_serializer');

        $response = $this->orderService->getOrderById($id);

        return new Response($serializer->serialize($response, "json"), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/v1/order", name="create_order")
     * @param Request $request
     * @return Response
     */
    public function createOrder(Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        $serializer = $this->get('jms_serializer');

        $response = $this->orderService->createOrder($request, $entityManager);
        return new Response($serializer->serialize($response, "json"), Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/v1/order/{id}", name="update_order")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function updateOrder(int $id, Request $request)
    {
        $serializer = $this->get('jms_serializer');
        $entityManager = $this->getDoctrine()->getManager();

        $response = $this->orderService->updateOrder($id, $request, $entityManager);
        return new Response($serializer->serialize($response, "json"), Response::HTTP_OK);
    }
}
