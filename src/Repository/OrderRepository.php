<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findByOrderId($orderId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT o.*, s.name as statusName  
        FROM `order` o
        INNER JOIN status s on o.status_id = s.id
        WHERE o.id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $orderId]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetch();
    }
}
