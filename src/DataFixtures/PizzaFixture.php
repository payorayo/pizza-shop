<?php

namespace App\DataFixtures;

use App\Entity\Pizza;
use App\Entity\Topping;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PizzaFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pizza1 = new Pizza();
        $pizza1->setName('Classic Veg Pizza');
        $pizza1->setDescription('Loaded with onion & capsicum');
        $manager->persist($pizza1);

        $pizza2 = new Pizza();
        $pizza2->setName('Fresh Veggies Spl Pizza');
        $pizza2->setDescription('Onion, capsicum, mushroom');
        $manager->persist($pizza2);

        $pizza3 = new Pizza();
        $pizza3->setName('Mexican Delight');
        $pizza3->setDescription('BBQ Chicken, Tomato, mushroom');
        $manager->persist($pizza3);

        $topping1 = new Topping();
        $topping1->setName('Onion');
        $topping1->addPizza($pizza1);
        $topping1->addPizza($pizza2);
        $manager->persist($topping1);

        $topping2 = new Topping();
        $topping2->setName('Capsicum');
        $topping2->addPizza($pizza1);
        $topping2->addPizza($pizza2);
        $manager->persist($topping2);

        $topping3 = new Topping();
        $topping3->setName('Mushroom');
        $topping3->addPizza($pizza2);
        $topping3->addPizza($pizza3);
        $manager->persist($topping3);

        $topping4 = new Topping();
        $topping4->setName('BBQ Chicken');
        $topping4->addPizza($pizza3);
        $manager->persist($topping4);

        $topping5 = new Topping();
        $topping5->setName('Tomato');
        $topping5->addPizza($pizza3);
        $manager->persist($topping5);


        $manager->flush();
    }

}
