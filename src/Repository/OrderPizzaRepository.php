<?php

namespace App\Repository;

use App\Entity\OrderPizza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderPizza|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderPizza|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderPizza[]    findAll()
 * @method OrderPizza[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderPizzaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderPizza::class);
    }

    public function findAllPizzasByOrderId($orderId): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT  op.pizza_id, p.name as pizza_name, op.amount, op.size_id, s.name as size_name  
        FROM order_pizza op
        INNER JOIN pizza p on op.pizza_id = p.id
        INNER JOIN `size` s on op.size_id = s.id
        WHERE op.ordercurrent_id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $orderId]);

        return $stmt->fetchAll();
    }

    public function deleteByOrderId($orderId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'delete  
        FROM order_pizza
        WHERE ordercurrent_id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $orderId]);
    }
}
