<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderPizza;
use App\Repository\OrderPizzaRepository;
use App\Repository\OrderRepository;
use App\Repository\PizzaRepository;
use App\Repository\SizeRepository;
use App\Repository\StatusRepository;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;

class OrderService
{

    private $orderRepository;
    private $statusRepository;
    private $pizzaRepository;
    private $sizeRepository;
    private $orderPizzaRepository;

    public function __construct(OrderRepository $orderRepository,
                                StatusRepository $statusRepository,
                                PizzaRepository $pizzaRepository,
                                SizeRepository $sizeRepository,
                                OrderPizzaRepository $orderPizzaRepository){

        $this->orderRepository = $orderRepository;
        $this->statusRepository = $statusRepository;
        $this->pizzaRepository = $pizzaRepository;
        $this->sizeRepository = $sizeRepository;
        $this->orderPizzaRepository = $orderPizzaRepository;
    }

    public function getOrderById($id): array
    {
        $order = $this->orderRepository->findByOrderId($id);
        if(!$order){
            throw new \InvalidArgumentException(Constants::ID_ERROR);
        }

        return $this->getResponseByArray($order);
    }

    public function createOrder(Request $request, $entityManager): array
    {
        $pizzas = $request->get('pizzas', null);
        if(!$this->validatePizzaRequest($pizzas)) {
            throw new \InvalidArgumentException(Constants::PIZZAS_ERROR);
        }

        $status = $this->statusRepository->find(Constants::STATUS_DEFAULT);
        $order = new Order();
        $order->setCreatedAt(Carbon::now());
        $order->setStatus($status);
        $entityManager->persist($order);

        $this->addNewPizzas($pizzas, $order, $entityManager);

        $entityManager->flush();
        return $this->getResponseByClass($order);
    }

    public function updateOrder($id, Request $request, $entityManager): array
    {
        $order = $this->orderRepository->find($id);
        if(!$order){
            throw new \InvalidArgumentException(Constants::ID_ERROR);
        }
        $pizzas = $request->get('pizzas', null);
        if(!$this->validatePizzaRequest($pizzas)) {
            throw new \InvalidArgumentException(Constants::PIZZAS_ERROR);
        }

        $requestStatus = $request->get('status', 0);
        $status = $this->statusRepository->find($requestStatus);
        if(!$status){
            throw new \InvalidArgumentException(Constants::STATUS_ERROR);
        }

        $order->setStatus($status);
        $order->setUpdatedAt(Carbon::now());
        $entityManager->persist($order);
        $this->orderPizzaRepository->deleteByOrderId($id);

        $this->addNewPizzas($pizzas, $order, $entityManager);
        $entityManager->flush();
        return $this->getResponseByClass($order);
    }


    private function validatePizzaRequest($pizzas): bool
    {
        if(!$pizzas) {
            return false;
        }

        foreach ($pizzas as $item) {
            $pizza = !empty($item["pizza_id"]) ? $this->pizzaRepository->find($item["pizza_id"]) : '';
            $size = !empty($item["size_id"]) ? $this->sizeRepository->find($item["size_id"]) : '';
            $amount = !empty($item["amount"]) ?intval($item["amount"]) : '';
            if(!$pizza || !$size || !$amount){
                return false;
            }
        }

        return true;
    }

    private function addNewPizzas($pizzas, $order, $entityManager): void
    {
        foreach ($pizzas as $item) {
            $orderPizza = new OrderPizza();
            $pizza = $this->pizzaRepository->find($item["pizza_id"]);
            $size = $this->sizeRepository->find($item["size_id"]);
            $amount = intval($item["amount"]);

            $orderPizza->setPizza($pizza);
            $orderPizza->setSize($size);
            $orderPizza->setOrdercurrent($order);
            $orderPizza->setAmount($amount);
            $entityManager->persist($orderPizza);
        }
    }

    private function getResponseByClass($order): array
    {
        return [
            "data" => [
                "id" => $order->getId(),
                "createdAt" => $order->getCreatedAt(),
                "status" => $order->getStatus()->getName(),
                "pizzas" => $this->orderPizzaRepository->findAllPizzasByOrderId($order->getId())
            ]
        ];
    }

    private function getResponseByArray($order): array
    {
        return  [
            "data" => [
                "id" => $order["id"],
                "createdAt" => $order["created_at"],
                "status" => $order["statusName"],
                "pizzas" => $this->orderPizzaRepository->findAllPizzasByOrderId($order["id"])
            ]
        ];
    }
}